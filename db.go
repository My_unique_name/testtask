package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

// Функция вставки токенов в БД
func insertTokenToDb(refreshToken string, id string) (interface{}, error){
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config["atlasCluster"]))
	if err != nil {
		return nil, err
	}
	var result interface{}
	collection := client.Database("newDb").Collection("refreshTokens")
	err = client.UseSession(ctx, func(sessionContext mongo.SessionContext) error {
		err := sessionContext.StartTransaction()
		if err != nil {
			return err
		}
		queryResult, err := collection.InsertOne(sessionContext,
			bson.D{
				{"userID", id},
				{"refreshToken", refreshToken}})
		if err != nil {
			if err := sessionContext.AbortTransaction(sessionContext); err != nil {
				return err
			}
		} else {
			result = queryResult.InsertedID
			if err := sessionContext.CommitTransaction(sessionContext); err != nil {
				return err
			}
		}
		return nil
	})
	defer client.Disconnect(ctx)
	return result, nil
}
// Удаление конкретного токена из БД
func deleteByTokenFromDb(refreshToken string) (interface{}, error){
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config["atlasCluster"]))
	if err != nil {
		return nil, err
	}
	var result interface{}
	collection := client.Database("newDb").Collection("refreshTokens")
	err = client.UseSession(ctx, func(sessionContext mongo.SessionContext) error {
		err := sessionContext.StartTransaction()
		if err != nil {
			return err
		}
		queryResult, err := collection.DeleteOne(sessionContext, bson.M{"refreshToken": refreshToken})
		if err != nil {
			if err := sessionContext.AbortTransaction(sessionContext); err != nil {
				return err
			}
		} else {
			result = queryResult.DeletedCount
			if err := sessionContext.CommitTransaction(sessionContext); err != nil {
				return err
			}
		}
		return nil
	})
	defer client.Disconnect(ctx)
	return result, nil
}
// Удаление всех токенов связанных с конкртным пользователем
func deleteByUserIdFromDb(userId string) (interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config["atlasCluster"]))
	if err != nil {
		return nil, err
	}
	var result interface{}
	collection := client.Database("newDb").Collection("refreshTokens")
	err = client.UseSession(ctx, func(sessionContext mongo.SessionContext) error {
		err := sessionContext.StartTransaction()
		if err != nil {
			return err
		}
		queryResult, err := collection.DeleteMany(sessionContext, bson.M{"userID": userId})
		if err != nil {
			if err := sessionContext.AbortTransaction(sessionContext); err != nil {
				return err
			}
		} else {
			result = queryResult.DeletedCount
			if err := sessionContext.CommitTransaction(sessionContext); err != nil {
				return err
			}
		}
		return nil
	})
	defer client.Disconnect(ctx)
	return result, nil
}
// Создание новой коллекции
func createCollection(dbName string, collectionName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config["atlasCluster"]))
	if err != nil {
		return err
	}
	result := client.Database(dbName).CreateCollection(ctx, collectionName)
	if result != nil {
		return err
	}
	defer client.Disconnect(ctx)
	return nil
}
// Удаление коллекции
func dropCollection(dbName string, collectionName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config["atlasCluster"]))
	if err != nil {
		return err
	}
	result := client.Database(dbName).Collection(collectionName).Drop(ctx)
	if result != nil {
		return err
	}
	defer client.Disconnect(ctx)
	return nil
}
// Поиск и обновление рефреш токена
func findOneAndUpdate(userId string, oldRefreshToken string, newRefreshToken string) (interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config["atlasCluster"]))
	if err != nil {
		return nil, err
	}
	var result interface{}
	collection := client.Database("newDb").Collection("refreshTokens")
	err = client.UseSession(ctx, func(sessionContext mongo.SessionContext) error {
		err := sessionContext.StartTransaction()
		if err != nil {
			return err
		}
		query := collection.FindOneAndUpdate(sessionContext,
			bson.M{
				"userID": userId,
				"refreshToken": oldRefreshToken},
			bson.D{
				{"$set", bson.D{{"refreshToken", newRefreshToken}}},
			})

		if query != nil {
			query.Decode(&result)
		}
		if query.Err() != nil {
			if err := sessionContext.AbortTransaction(sessionContext); err != nil {
				return err
			}
			return err
		} else {
			if err := sessionContext.CommitTransaction(sessionContext); err != nil {
				return err
			}
		}
		return nil
	})
	defer client.Disconnect(ctx)
	return result, nil
}
// Подготовка БД
func initializeDb() error{
	if err := dropCollection("newDb", "refreshTokens"); err != nil {
		return err
	}
	if err := createCollection("newDb", "refreshTokens"); err != nil {
		return err
	}
	return nil
}