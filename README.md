Пояснения к работе:
=====================

    1) Приложение находится тут https://evening-crag-49322.herokuapp.com/
    2) Refresh token в данной работе является JWT, он также как и Access token имеет 3 поля claims:
    -   user_id - идентификатор пользователя
    -   expired - время когда токен перестанет быть валидным
    -   pair_id - идентификатор пары access-refresh (состоит из user_id + серверное время создания пары токенов (unix.nano))
    3) При рефреше токенов, серверу должны быть отправлены 2 экземпляря access и refresh токены, для проверки
    их связанности.
    4) Передаются токены через строку запроса, т.к. не особо понял как все таки правильно их передавать
    (сделать кастомный заголовок, 2 токена в Authorization и тд.)
    5) В работе используется Mongo Atlas:
    -   Passwords: 1234
    -   User: Admin
    -   Команда для подключения: mongo "mongodb+srv://cluster.dhwpc.mongodb.net/<newDb>" --username Admin
-----------------------------------
Четыре REST маршрута:
=====================
Пункт 1:
-----------------------------------
        Первый маршрут выдает пару Access, Refresh токенов для пользователя с
        идентификатором (GUID) указанным в параметре запроса.
    Пример запроса:
        Метод (GET)
        https://evening-crag-49322.herokuapp.com/get-tokens/?id=4
    Пример ответа:
        {
            "accessToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDUwMjgsInBhaXJfaWQiOiI0LjE2MDY2NDQ5NjgxMDE1NjkxMTQiLCJ1c2VyX2lkIjoiNCJ9.77xlTfEkpACIVA6-vO4sJ38wM_YJiaUH4DqYH1SkySprZ3JGXAXWfypu1THsUQR0hPrg-lEpoHVQQZbWJJ8Smw",
            "refreshToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDY3NjgsInBhaXJfaWQiOiI0LjE2MDY2NDQ5NjgxMDE1NjkxMTQiLCJ1c2VyX2lkIjoiNCJ9.zEGnpJXX_JqvaZPtgXz9e-4G-BXw-DMzlprRULIo5soS82hzNQ0NZebcr-FMtRlJqOWtgx5_7ZlPwC-Dgj9WWw"
        }
-----------------------------------        
Пункт 2:
-----------------------------------
        Второй маршрут выполняет Refresh операцию на пару Access, Refresh токенов.
    Пример запроса:
        Метод (GET)
        https://evening-crag-49322.herokuapp.com/refresh/?accessToken=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDU1MjksInBhaXJfaWQiOiI1LjE2MDY2NDU0NjkxOTgxNDA4NjAiLCJ1c2VyX2lkIjoiNSJ9.eg6FZrCTZf_EkwfCXpqUjYIB2cUiTHgu7mEhj09TXBA7Bv-LGKUuKUlTmb0sWCgQXDtWHuPinJgok3BhQNL6sQ&refreshToken=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDcyNjksInBhaXJfaWQiOiI1LjE2MDY2NDU0NjkxOTgxNDA4NjAiLCJ1c2VyX2lkIjoiNSJ9.iGkQ1PFCl_gSwnrBm7O0SEGkY0Pxh4GLWGt1ghbzdhZlkAWBRbXw0V2Wuc-tyTVxVk9KmsnxdFU1-nLu7-TEZA
    Пример ответа:
        {
            "accessToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDU2NzMsInBhaXJfaWQiOiI1LjE2MDY2NDU2MTM4MzA4NzQ5OTkiLCJ1c2VyX2lkIjoiNSJ9.5_snbS-HWTHQMWwieH7qE8JvNs-Os3CusSElYhb4VAbDFy17Qks0j3sGYNN4r93T8t6bcwF-0SKNOhhqT8MoNA",
            "refreshToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDc0MTMsInBhaXJfaWQiOiI1LjE2MDY2NDU2MTM4MzA4NzQ5OTkiLCJ1c2VyX2lkIjoiNSJ9.j2_AJaRSBWDkJBUB8TK4BkVlxwRwMGUS9lAVqFMRYY6YKWabLZZB_ofezYfsnkgfgDIPkIj8m7UGjLFqTaWyJg"
        }
-----------------------------------
Пункт 3:
-----------------------------------
        Третий маршрут удаляет конкретный Refresh токен из базы.
    Пример запроса:
        Метод (DELETE)
        https://evening-crag-49322.herokuapp.com/delete-by-token/?refreshToken=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDcyNTUsInBhaXJfaWQiOiI0LjE2MDY2NDU0NTU5OTU0MDg2MzgiLCJ1c2VyX2lkIjoiNCJ9.QrIo81otM4hGRwU-b8igA7gVht3m8UWExNDFX_H04DhI6h1ypWx6er2s72iHNAXYOjce9iogTUNnBqOHuXMhYQ
    Пример ответа:
        {"delete count":"0"}
 -----------------------------------
Пункт 4:
-----------------------------------
        Четвертый маршрут удаляет все Refresh токены из базы для конкретного пользователя.
    Пример запроса:
        Метод (DELETE)
        https://evening-crag-49322.herokuapp.com/delete-by-user-id/?id=4
    Пример ответа:
        {"delete count":"3"}
-----------------------------------
Дополнительно:
-----------------------------------
        Данный маршрут проверяет целостность данных и время жизни токена. (сделано для тестирования)
    Пример запроса:
        Метод (GET)
        https://evening-crag-49322.herokuapp.com/check-token/?token=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHBpcmUiOjE2MDY2NDU2NzMsInBhaXJfaWQiOiI1LjE2MDY2NDU2MTM4MzA4NzQ5OTkiLCJ1c2VyX2lkIjoiNSJ9.5_snbS-HWTHQMWwieH7qE8JvNs-Os3CusSElYhb4VAbDFy17Qks0j3sGYNN4r93T8t6bcwF-0SKNOhhqT8MoNA
    Пример ответа:
        {"information":"token is valid"}

-----------------------------------
P.S. До этого на Golang никогда не программировал.
