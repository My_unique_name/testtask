package main

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var config = map[string]string {
	"secretKey": "SecretKey",
	"atlasCluster": "mongodb+srv://Admin:1234@cluster.dhwpc.mongodb.net/<newDb>?retryWrites=true&w=majority",
}

// Декодирование токена и проверка целостности данных
func decodeToken(tokenString string) (*jwt.Token, error){
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("jwt parse error")
		}
		return []byte(config["secretKey"]), nil
	})
	if token == nil {
		return nil, fmt.Errorf("token is not valid")
	}
	if token.Claims.(jwt.MapClaims)["expire"] == nil || token.Claims.(jwt.MapClaims)["user_id"] == nil{
		return nil, fmt.Errorf("token is not valid")
	}
	if err != nil {
		return token, nil
	}
	return token, err
}
// Создание токена
func createToken(userId string, expire time.Duration) *jwt.Token {
	token := jwt.New(jwt.SigningMethodHS512)
	claims := token.Claims.(jwt.MapClaims)
	claims["user_id"] = userId
	claims["expire"] = time.Now().Add(expire).Unix()
	return token
}
// Создание пары токенов
func createTokenPair(userId string) (*jwt.Token, *jwt.Token){
	accessToken := createToken(userId, time.Minute * 1)
	refreshToken := createToken(userId, time.Minute * 30)
	pairId := userId + "." + strconv.FormatInt(time.Now().UnixNano(), 10)
	accessToken.Claims.(jwt.MapClaims)["pair_id"] = pairId
	refreshToken.Claims.(jwt.MapClaims)["pair_id"] = pairId
	return accessToken, refreshToken
}
// Выдача токенов пользователю по ID
func getTokens(w http.ResponseWriter, r *http.Request){
	// Проверка наличия ID в строке запроса
	id := r.FormValue("id")
	if len(id) == 0 {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]string {"error": "missing ID"})
		return
	}
	accessToken, refreshToken := createTokenPair(id)
	accessTokenString, aError := accessToken.SignedString([]byte(config["secretKey"]))
	refreshTokenString, rError := refreshToken.SignedString([]byte(config["secretKey"]))
	if aError != nil || rError != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]string{ "error:": "can`t create tokens"})
		return
	}
	// Добавление refresh token`a в базу данных
	if _, err := insertTokenToDb(refreshTokenString, id); err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]string {"error": err.Error()})
		return
	}
	// Отдаем токены
	json.NewEncoder(w).Encode(map[string]string{
		"accessToken": accessTokenString,
		"refreshToken": refreshTokenString,
	})
}
// Проверка времени жизни токена
func tokenVerification(tokenString string) (*jwt.Token, error) {
	token, err := decodeToken(tokenString)
	if err != nil {
		return nil, err
	}
	// Проверка времени жизни токена
	if int64(token.Claims.(jwt.MapClaims)["expire"].(float64)) - time.Now().Unix() < 0 {
		return nil, fmt.Errorf("token has expired")
	}
	if token.Valid {
		return token, nil
	}
	return nil, fmt.Errorf("token is not valid")
}
// Обновление токенов
func refreshToken(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("refreshToken") == "" || r.FormValue("accessToken") == "" {
		json.NewEncoder(w).Encode(map[string]string {"error": "missing tokens"})
		return
	}
	refreshToken, err := tokenVerification(r.FormValue("refreshToken"))
	if err != nil {
		json.NewEncoder(w).Encode(map[string]string {"error": err.Error()})
		return
	}
	accessToken, err := decodeToken(r.FormValue("accessToken"))
	if err != nil {
		json.NewEncoder(w).Encode(map[string]string {"error": err.Error()})
		return
	}
	// Проверяем связь токенов по pair_id
	if refreshToken.Claims.(jwt.MapClaims)["pair_id"] != accessToken.Claims.(jwt.MapClaims)["pair_id"] {
		json.NewEncoder(w).Encode(map[string]string {"error": "tokens are not linked"})
		return
	}
	id := refreshToken.Claims.(jwt.MapClaims)["user_id"].(string)

	newAccessToken, newRefreshToken := createTokenPair(id)
	accessTokenString, aError := newAccessToken.SignedString([]byte(config["secretKey"]))
	refreshTokenString, rError := newRefreshToken.SignedString([]byte(config["secretKey"]))

	result, err := findOneAndUpdate(id, r.FormValue("refreshToken"), refreshTokenString)
	if err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]string{ "error": err.Error()})
		return
	}
	if result == nil {
		w.WriteHeader(404)
		json.NewEncoder(w).Encode(map[string]string{ "error": "token not found"})
		return
	}
	if aError != nil || rError != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]string{ "error": "can`t refresh tokens"})
		return
	}
	json.NewEncoder(w).Encode(map[string]string{
		"accessToken": accessTokenString,
		"refreshToken": refreshTokenString,
	})
}
// Удаление конкретного токена в БД
func deleteByToken(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("refreshToken")
	if len(token) == 0 {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]string {"error": "missing token"})
		return
	}
	_, err := tokenVerification(r.FormValue("refreshToken"))
	if err != nil {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]string {"error": err.Error()})
		return
	}
	result, err := deleteByTokenFromDb(r.FormValue("refreshToken"))
		if err != nil{
			w.WriteHeader(404)
			json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
			return
		}
		// Если не был удален токен возращаем 404 (повторное использование)
		if result.(int64) == 0 {
			w.WriteHeader(404)
		}
	json.NewEncoder(w).Encode(map[string]string{"delete count": strconv.FormatInt(result.(int64), 10)})
}
// Удаление всех токенов определенного пользователя
func deleteByUserId(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("id")
	if len(token) == 0 {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]string {"error": "missing id"})
		return
	}
	result, err := deleteByUserIdFromDb(r.FormValue("id"))
	if err != nil{
		w.WriteHeader(404)
		json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
		return
	}
	// Если ни один токен не был удален возращаем 404
	if result.(int64) == 0 {
		w.WriteHeader(404)
	}
	json.NewEncoder(w).Encode(map[string]string{"delete count": strconv.FormatInt(result.(int64), 10)})
}
// Проверка действительности токена
func checkToken(w http.ResponseWriter, r *http.Request) {
	tokenString := r.FormValue("token")
	if len(tokenString) == 0 {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]string {"error": "missing token"})
		return
	}
	_, err := tokenVerification(tokenString)
	if err != nil {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]string {"error": err.Error()})
		return
	}
	json.NewEncoder(w).Encode(map[string]string {"information": "token is valid"})
	return
}

func handleRequests() {
	serverPort := os.Getenv("PORT")
	if serverPort == "" {
		serverPort = "8080"
	}
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.Host("https://evening-crag-49322.herokuapp.com")
	// маршрут для проверки токена
	myRouter.HandleFunc("/check-token", checkToken).Methods("GET")
	// маршрут для получения двух токенов по guid
	myRouter.HandleFunc("/get-tokens", getTokens).Methods("GET")
	// маршрут для обновления токенов
	myRouter.HandleFunc("/refresh", refreshToken).Methods("GET")
	// маршрут для удаления refresh token`a с базы данных
	myRouter.HandleFunc("/delete-by-token/", deleteByToken).Methods("DELETE")
	// маршрут для удаления refresh token`a по идентификатору пользователя
	myRouter.HandleFunc("/delete-by-user-id/", deleteByUserId).Methods("DELETE")
	http.Handle("/", myRouter)
	log.Fatal(http.ListenAndServe(":" + serverPort, nil))
}

func main() {
	fmt.Println("Server has started")
	// Перед запуском сервера очищаем базу данных для тестирования
	if err := initializeDb(); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Database has initialized")
	handleRequests()
}

